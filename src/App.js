import React, {useEffect, useState} from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './components/layouts/Navbar';
import Catalog from './components/Catalog';

//Form
import AdminPanel from './components/layouts/AdminPanel'
import RegisterForm from './components/forms/RegisterForm'
import LoginForm from './components/forms/LoginForm'

function App() {
  // states
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState ([])

// component updates go through here
  useEffect( () => {
   fetch("http://localhost:3001/products")
   .then(res => res.json())
   .then(data => {
     setProducts([...data])
   })

   fetch("http://localhost:3001/categories")
   .then(res => res.json())
   .then(data => {
     setCategories([...data])
   })
  }, []);

  return (
    <Router>
      <div>
          <Navbar/>
          <Switch>
            <Route exact path='/'>
              <Catalog products={products}/>
            </Route>
            <Route path='/admin-panel'>
              <AdminPanel categories={categories}/>
            </Route>
            <Route path='/register'>
              <RegisterForm/>
            </Route>
            <Route path='/login'>
              <LoginForm/>
            </Route>
          </Switch>
      </div>
    </Router>
  );
}

export default App;
