import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
	return (
		<Fragment>
			<nav className="navbar navbar-dark bg-dark navbar-expand-sm">
				<Link to="/" className="navbar-brand">PUSHCART</Link>
				<button 
					className='navbar-toggler'
					type='button'
					data-toggle='collapse'
					data-target='#navbarNav'
				>
				<span className='navbar-toggler-icon'></span>
				</button>
				<div className="collapse navbar-collapse" id='navbarNav'>
					<ul className='navbar-nav ml-auto'>
						<li className='nav-item'>
							<Link to='/' className='nav-link'>Catalog</Link>
						</li>
						<li className='nav-item'>
							<Link to='/admin-panel' className='nav-link'>Admin Panel</Link>
						</li>
						<li className='nav-item'>
							<Link to='/register' className='nav-link'>Register</Link>
						</li>
						<li className='nav-item'>
							<Link to='/login' className='nav-link'>Login</Link>
						</li>
					</ul>
				</div>
			</nav>
		</Fragment>
	);
}

export default Navbar;