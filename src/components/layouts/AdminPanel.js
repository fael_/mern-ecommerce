import React from 'react'
import AddProduct from '../forms/AddProductForm';
import EditProduct from '../forms/EditProductForm';
import AddCategory from '../forms/AddCategory';
import EditCategory from '../forms/EditCategory';
import RemoveCategory from '../forms/DeleteCategory';

function AdminPanel({categories}){
	return(
		<div class="container bg-warning">
			<div class="row text-success">
				<div class="col col-sm-12 col-md-8">
					<div class="row">
					<div className="col-12">
						<AddProduct/>
					</div>
					</div>
					<div class="row">
					<div className="col-12">
						<EditProduct/>
					</div>
					</div>
				</div>
				<div class="col col-sm-12 col-md-4">
					<div class="row sticky-top">
					<div className="col-12">
						<AddCategory/>
					</div>
					<div className="col-12">
						<EditCategory categories={categories}/>
					</div>
					<div className="col-12">
						<RemoveCategory categories={categories}/>
					</div>
					</div>
					</div>
				</div>
			</div>
	)
}

export default AdminPanel