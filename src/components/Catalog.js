import React from 'react';

const Catalog = ({products}) => {
// console.log(products)
	return(
		<div className='container'>
			<div className='row'>
				{products.map(product => (
					<div className="col-4">
						<div className='card mt-4'>
							<img src={"http://localhost:3001" + product.image} alt=""/>
							<div className="card-body">
								<h5 className="card-title">{product.name}</h5>
								<p className="card-text">{product.categoryId}</p>
								<p className="card-text">{product.price}</p>
								<p className="card-text">{product.description}</p>
							</div>
						</div>
					</div>
				))}
			</div>
		</div>
	);
}

export default Catalog;