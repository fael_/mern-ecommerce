import React, {useState} from 'react'


const RemoveCategory = ({categories}) => {
	const [selectedCategory, setSelectedCategory] = useState({
	name : null,
	id : null
})
const handleChangeSelected = (e) => {
	let categorySelected = categories.find(category => {
		if(category._id == e.target.value){
			return category.name
		}
})
	setSelectedCategory({
		id : e.target.value,
		name : categorySelected.name
})
}
const handleChangeName = (e) =>{
	setSelectedCategory({
	...selectedCategory,[e.target.name] : e.target.value
})
}

const handleRemoveCategory = (e) =>{
	e.preventDefault();
	fetch("http://localhost:3001/categories/" + selectedCategory.id,{
		method : "DELETE",
		body : JSON.stringify({name : selectedCategory.name}),
		headers : {
			"Content-Type" : "application/json",
			"Authorization" : localStorage.getItem('token'),
		}
	})
	.then(data=>data.json())
	.then(result => console.log(result))
}
	return(
		<div>
			<div className="container">
				<div className="row">
					<div className="col">
						<h2 className="mt-5">Remove Category</h2>
						<form onSubmit={handleRemoveCategory}>
							<select name="category" id="category" className="form-control mt-3" onChange={handleChangeSelected}>
								{ categories.map(category => {
									return (
										<option value={category._id}>{category.name}</option>
									)
								})
								}
							</select>
							<button className="form-control btn btn-danger">Delete</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}

export default RemoveCategory;