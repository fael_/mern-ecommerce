import React from 'react'

const EditProduct = ({categories}) => {
	return(
		<div>
			<h2 className="mt-5">Edit Product</h2>
				<form onSubmit="">
					<img src="" alt="Product Image Here" className="form-control mt-3"/>
					<input type="text" name="name" id="name" placeholder="Product Name" className="form-control my-3"/>
					<input type="text" name="price" id="price" placeholder="Product Price" className="form-control mb-3"/>
					<select name="category" id="category" className="form-control mb-3">
						<option value="category.id">Category 1</option>
						<option value="category.id">Category 2</option>
					</select>
					<textarea name="description" id="description" className="w-100 mb-3" rows="15" placeholder="Product Description"></textarea>
					<button className="form-control btn btn-primary">Save Changes</button>
				</form>
		</div>
	);
}

export default EditProduct;