import React, { useState } from 'react'

const RegisterForm = () => {

	const [formData, setFormData] = useState({
		firstname : '',
		lastname : '',
		email : '',
		password : '',
		confirmPassword : ''
	});

	const { firstname, lastname, email, password, confirmPassword } = formData;

	const onChangeHandler = e => {
		setFormData({
			...formData,
			[e.target.name] : e.target.value
		})
	}

	const handleRegister = e => {
		e.preventDefault();
		if(password !== confirmPassword) {
			alert("Passwords do not match")
		} else {
			fetch("http://localhost:3001/users/register", {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(formData)
			})
			.then(data => data.json())
			.then(user => { 
				console.log(user.message)
				setFormData({
					firstname : '',
					lastname : '',
					email : '',
					password : '',
					confirmPassword : ''
				})
				if (user.message) {
					let element = document.getElementById('message')
					element.innerHTML = user.message
					element.classList.toggle('d-none')
					setTimeout(function(){
						element.classList.toggle('d-none')
					},3000)
				}
				if (user.successMessage) {
					let element = document.getElementById('smessage')
					element.innerHTML = user.successMessage
					element.classList.toggle('d-none')
					setTimeout(function(){
						element.classList.toggle('d-none')
					},3000)
				}
			})	
		}
	}

	return(
		<div className='container'>
			<div className='col-lg-6 mx-auto pt-4'>
{/*				{JSON.stringify(formData)}*/}
				<div className='alert alert-danger d-none' role='alert' id='message'>
				</div>
				<div className='alert alert-success d-none' role='alert' id='smessage'>
				</div>
				
				<h2>Register Page</h2>
				<form onSubmit={ e => handleRegister(e) }>
					<div className='form-group'>
						<label htmlFor='firstname'>Firstname</label>
						<input
							type='text'
							className='form-control'
							id='firstname'
							name='firstname'
							value={firstname}
							onChange={ e => onChangeHandler(e) }
						/>
						<h3>{firstname}</h3>
					</div>
					<div className='form-group'>
						<label htmlFor='lastname'>Lastname</label>
						<input
							type='text'
							className='form-control'
							id='lastname'
							name='lastname'
							value={lastname}
							onChange={ e => onChangeHandler(e) }
						/>
						<h3>{lastname}</h3>
					</div>
					<div className='form-group'>
						<label htmlFor='email'>Email</label>
						<input
							type='email'
							className='form-control'
							id='email'
							name='email'
							value={email}
							onChange={ e => onChangeHandler(e) }
						/>
					</div>
					<div className='form-group'>
						<label htmlFor='password'>Password</label>
						<input
							type='password'
							className='form-control'
							id='password'
							name='password'
							value={password}
							onChange={ e => onChangeHandler(e) }
						/>
					</div>
					<div className='form-group'>
						<label htmlFor='confirmPassword'>Confirm Password</label>
						<input
							type='password'
							className='form-control'
							id='confirmPassword'
							name='confirmPassword'
							value={confirmPassword}
							onChange={ e => onChangeHandler(e) }
						/>
					</div>
					<button type='submit' className='btn btn-primary'>Submit</button>
				</form>
			</div>
		</div>
	)
}

export default RegisterForm;