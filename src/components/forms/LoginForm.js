import React, {useState} from 'react'

const LoginForm = () => {
	const [formData, setFormData] = useState({
	email : null,
	password : null
})

const [result, setResult] = useState({
	message: null,
	successful: null
})

		const handleChange = e => {
		setFormData({
			...formData,
			[e.target.name] : e.target.value
		})
	}

const handleLogin = e => {
	e.preventDefault();

	let data = {
		email : formData.email,
		password : formData.password
	}

	fetch('http://localhost:3001/users/login',{
		method : "POST",
		body : JSON.stringify(data),
		headers : {
			"Content-Type" : "application/json"
		}
	})
	.then(data => data.json())
	.then(user => {
		console.log(user)
		if(user.token){
			setResult({
				successful : true,
				message : user.message
			})
			localStorage.setItem('user',JSON.stringify(user.user));
			localStorage.setItem('token',"Bearer " +  user.token);
		}
		else{
			setResult({
				successful : false,
				message : user.message
			})
		}
	})
}
const resultMsg =() => {
	let classList;
	if(result.successful == true){
		classList = 'alert alert-success'
	}
	else{
		classList = 'alert alert-warning'
	}
	return(
		<div className={classList}>
			{result.message}
		</div>
	)
}
	return(
		<div>
			<div class="container text-center">
				<div class="row">
					<div class="col-12">
						<form onSubmit={handleLogin} className="mt-3 bg-warning">
							<h2 className="bg-dark text-white">Login Form</h2>
							{result.successful == null ? "" : resultMsg()}
							<label htmlFor="email" className="mt-3">
								Email:
							</label>
							<input type="email" name="email" id="email" className="form-control w-50 mx-auto" onChange={handleChange}/>
							<label htmlFor="password" className="mt-3">
								Password:
							</label>
							<input type="password" name="password" id="password" className="form-control w-50 mx-auto" onChange={handleChange}/>
							<button className="btn btn-success mt-3">Log In</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	)
}

export default LoginForm