import React, {useState} from 'react'

//init variables
const EditCategory = ({categories}) => {
	const [selectedCategory, setSelectedCategory] = useState({
	name : null,
	id : null,
})
//dynamic name state
const handleChangeSelected = (e) => {
	let categorySelected = categories.find(category => {
	return category._id == e.target.value
})
	setSelectedCategory({
		id : e.target.value,
		name : categorySelected.name
})
}
const handleChangeName = (e) =>{
	setSelectedCategory({
	...selectedCategory,[e.target.name] : e.target.value
})
}

//update
const handleEditCategory = (e) =>{
	e.preventDefault();
	fetch("http://localhost:3001/categories/" + selectedCategory.id,{
		method : "PUT",
		body : JSON.stringify({name : selectedCategory.name}),
		headers : {
			"Content-Type" : "application/json",
			"Authorization" : localStorage.getItem('token'),
		}
	})
	.then(data=>data.json())
	.then(result => console.log(result))
}
	return(
		<div>
			<div className="container">
				<div className="row">
					<div className="col">
						<h2 className="mt-5">Edit Category</h2>
						<form onSubmit={handleEditCategory}>
							<select onChange={handleChangeSelected} name="category" id="category" className="form-control">
								<option disabled selected>Select Category</option>
								{ categories.map(category => {
									return (
										<option value={category._id}>{category.name}</option>
									)
								})
								}
							</select>
							<input type="text" name="name" id="name" className="form-control mt-3" value={selectedCategory.name} onChange={handleChangeName}/>
							<button className="form-control btn btn-primary">Save Changes</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}

export default EditCategory;