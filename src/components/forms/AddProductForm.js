import React from 'react'

const AddProduct = () => {
	return(
		<div>
			<div className="container">
				<div className="row">
					<div className="col">
						<h2 className="mt-5">Add New Product</h2>
						<form onSubmit="">
							<input type="text" name="name" id="name" placeholder="Product Name" className="form-control my-3"/>
							<input type="number" name="price" id="price" placeholder="Product Price" className="form-control mb-3"/>
							<select name="category" id="category" className="form-control mb-3">
								<option value="category.id">Category 1</option>
								<option value="category.id">Category 2</option>
							</select>
							<input type="file" name="image" id="image" className="file mb-3" placeholder="Product Image"/>
							<textarea name="description" id="description" className="w-100 mb-3" rows="15" placeholder="Product Description"></textarea>
							<button className="form-control btn btn-success">Create Product</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}

export default AddProduct;