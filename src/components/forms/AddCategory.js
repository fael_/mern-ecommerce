import React, {useState} from 'react';

const AddCategory = () => {
	const [formData,setFormData] = useState("")
	const handleAddCategory = (e) => {
		e.preventDefault()
		let url = "http://localhost:3001/categories/";
		let data = {name:formData};
		let token = localStorage.getItem('token');

		fetch(url,{
			method : "POST",
			body : JSON.stringify(data),
			headers : {
				"Content-Type" : "application/json",
				"Authorization" : token
			}
		})
		.then(data => data.json())
		.then(category => console.log(category))
	}
	const handleCategoryChange = (e) => {
		setFormData(e.target.value)
	}
	return(
		<div>
			<div className="container">
				<div className="row">
					<div className="col">
						<h2 className="mt-5">Add New Category</h2>
						<form onSubmit={handleAddCategory}>
							<input 
							type="text" 
							name="category" 
							id="category" 
							className="form-control mt-3"
							onChange={handleCategoryChange}
							/>
							<button className="form-control btn btn-success">Create Category</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}

export default AddCategory;